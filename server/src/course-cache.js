const courseDb = require('./course-db')
const courseApi = require('./course-api')

//If a cache entry exist then check its time,
//If it is older than 24 hours then evict it
//If it is less than 24 hours then use it
//If a cache entry doesnt exist or it is evicted then get it from the api
//Add a date.toIsoString() time to lastCached attribute
async function get(courseId) {
  const result = await courseDb.get(courseId)

  if (result) {
    console.log(result);

    const timeLastCached = new Date(result.lastCached).getTime();
    const oneDayAgo = Date.now() - (1000 * 60 * 60 * 24);

    //Evict the entry
    if (timeLastCached > oneDayAgo) {
      return result;
    } else {
      await courseDb.evict(courseId);
    }
  }

  const course = await courseApi.get(courseId)

  if (course) {
    course.lastCached = new Date().toISOString();
    await courseDb.upsert(course);
  }

  return course
}

async function getCourses() {
  const courses = await courseApi.getCourses();

  return courses;
}

module.exports = {
  get,
  getCourses
}
