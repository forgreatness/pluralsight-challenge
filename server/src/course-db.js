const db = require('./sqlite-wrapper');

async function createTable() {
  const statement = `
    create table if not exists courses (
      id text primary key,
      title text not null,
      tags text not null,
      lastCached text not null
    );`

  return db.execute(statement);
}

async function get(courseId) {
  return db.get('select id, title, tags, lastCached from courses where id = $id;', { $id: courseId })
}

async function upsert(course) {
  const statement = `
    insert into courses (id, title, tags, lastCached)
    values ($id, $title, $tags, $lastCached)
    on conflict(id) do update set title = $title, lastCached = $lastCached, tags = $tags;`

  return db.execute(statement, { $id: course.id, $title: course.title, $tags: course.tags, $lastCached: course.lastCached });
}

async function evict(courseId) {
  const statement = `
    delete from courses where id = $id;
  `;

  return db.execute(statement, { $id: courseId });
}

module.exports = {
  createTable,
  get,
  upsert,
  evict
}
