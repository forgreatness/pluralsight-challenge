# Installation #

## Server ##
This assumes at least node 10 is installed

```
cd server
```

#### Install ####
```
npm install
```

#### Run back-end tests ####
```
npm run test                  # alternatively, npm run test:watch for watch mode
```

#### Run the API ####
```
npm start
```

## Client ##
```
cd client
```

#### Install ####
```
npm install
```

#### Run front-end tests ####
```
npm test
```

#### Run the client ####
```
npm start
```

## Documentation

### Client
#### Performance Decision
1. A lot of the components uses React.Fragment to composed multiple React elements within one single component because it saves rendering unnesscessary <div> container or unnesscessary list of elements.
2. Hooks that are used to fetch data from server are ran only on ComponentDidMount or when courseId change. This will help improve performance because data won't need to be fetch on every re-rendered.
#### Improvement in Performance
1. If there was additional time, I would incorporate Redux to manage the state for my application and hook any UI to it. For example, if there was Redux the we would only have to fetch once for all the courses, and the detail page can listen for the data using selectors. If the users want to make sure the data manage by the client internally is up to date with the datasources, we can give them the option to fetch using thunk action creators. Right now every detail page has to fetch their own data which is not as optimize as the proposed solution.
#### Project organization
1. All state, style, and ui related to a component are composed within a single file
2. Project is organizized based on a feature named "course"

### Server
#### Design Decision
1. I added a route to query all courses because it is useful for clients since there is a resource that can obtain all the data if it is needed
2. Every cache entry is tag with a date time isostring. The info will be use to evict old entry
