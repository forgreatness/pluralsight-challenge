/** @jsx jsx */
import React, { useState } from 'react';
import { jsx, css } from '@emotion/core';

export default ({ course }) => {
    const [ showTags, setShowTags ] = useState(false);

    const styles = css`  
        background: #222222;
        list-style-type: none;
        border: 1px solid #333333;
        margin: 20px;
        padding: 20px;
        display: inline-block;
        min-width: 150px;

        li > ul.tags-list {
            list-style-type: none;
        }

        li.Tags-List-Item:hover {
            cursor: pointer;
        }
    `;

    const tags = course.tags.split(", ");
    const tagsList = tags.map(tag => (
        <li>
            {tag}
        </li>
    ));

    return (
        <React.Fragment>
            <h1>Course Details</h1>
            <ul css={styles} className="course">
                <li><strong>ID:</strong> {course.id}</li>
                <li><strong>Title:</strong> {course.title}</li>
                <li onClick={() => setShowTags(showTags => !showTags)} className="Tags-List-Item">
                    <strong>Tags:</strong>
                    {showTags 
                        ? (<ul className="tags-list">
                            {tagsList}
                        </ul>)
                        : " ..."
                    }
                </li>
            </ul>
        </React.Fragment>
    );
}