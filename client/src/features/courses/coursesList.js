/** @jsx jsx */
import React, { useEffect, useState } from 'react';
import { jsx, css } from '@emotion/core';
import { Link } from 'react-router-dom';

export const CoursesList = () => {
    const [ courses, setCourses ] = useState([]);
    const [ isLoading, setIsLoading ] = useState(true);
    const [ error, setError ] = useState();

    useEffect(() => {
        fetch('http://localhost:5000/courses/')
            .then(res => {
                if (res.ok) {
                    return res.json();
                } else {
                    throw(res);
                }
            })
            .then(courses => {
                setCourses(courses);
                setError();
                setIsLoading(false);
            })
            .catch(err => {
                console.log(err);
                setError(err);
                setIsLoading(false);
            })
    }, []);

    const coursesList = courses.map(course => (
        <li key={course.id}>
            <h3>{course.title}</h3>
            <Link to={`/courses/${course.id}`}>Details</Link>
        </li>
    ));

    const styles = css`
        li {
            list-style-type: none;
            display: inline-block;
            border: 3px solid #4d79ff;
            padding: 20px 20px 10px;
            margin: 10px 10px;
            background: white;
            color: black;
            border-radius: 10px;
        }

        h3 {
            margin: 0px 0px 10px 0px;
        }

        a {
            float: right;
            border: 1px solid black;
            padding: 5px 5px;
            background-color: #4d79ff;
            color: white;
            text-decoration: none;
        }
    `;



    if (isLoading) {
        return (
            <div>
                Getting Data ...
            </div>
        );
    }

    if (error !== undefined) {
        return (
            <div>
                Error getting data - error.status
            </div>
        )
    }

    return (
        <div css={styles}>
            <h1>All Courses</h1>
            <ul>
                {coursesList}
            </ul>
        </div>
    );
}