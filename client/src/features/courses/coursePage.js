import React, { useEffect, useState } from 'react';

import CourseContainer from '../../components/courses/courseContainer';

export const CoursePage = ({ match }) => {
    const { courseId } = match.params;
    const [ isLoading, setIsLoading ] = useState(true);
    const [ error, setError ] = useState();
    const [ course, setCourse ] = useState({});

    useEffect(() => {
        fetch(`http://localhost:5000/courses/${courseId}`)
        .then(res => {
            if (res.ok) {
                return res.json();
            } else {
                console.log(res);
                throw(res.status);  
            }
        })
        .then(course => {
            setCourse(course);
            setIsLoading(false);
        })
        .catch(err => {
            setError(err);
            setIsLoading(false);
        });
    }, [courseId]);

    if (isLoading) {
        return (
            <div>
                Getting data ...
            </div>
        );
    }

    if (error !== undefined) {
        return (
            <div>
                Error getting data - {error}
            </div>
        )
    }

    return (
        <div>
            <CourseContainer course={course}/>
        </div>
    );
}