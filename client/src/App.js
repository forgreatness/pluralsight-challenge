import React from "react"
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link,
  Redirect
} from 'react-router-dom'

import { CoursesList } from './features/courses/coursesList';
import { CoursePage } from './features/courses/coursePage';
import "./App.css"

// I'm going to be using React Router to set up client side routing for my SPA
// The two most important route will be all courses page and the course detail page
export default () => {
  return (
    <Router>
      <div className="App">
        <Switch>
            <Route exact path="/"
              render={() => (
                <React.Fragment>
                  <h1>Welcome to Danh's Pluralsight Challenge</h1>
                  <Link to="/courses">Courses</Link>
                </React.Fragment>
              )}
            />
            <Route exact path="/courses" component={CoursesList} />
            <Route exact path="/courses/:courseId" component={CoursePage} />
            <Redirect to="/"></Redirect>
          </Switch>
      </div>
    </Router>
  );
}